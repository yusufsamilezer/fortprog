package com.company;

import java.util.Random;

public class Philosopher extends Thread {
    private Object leftStick;
    private Object rightStick;
    private int num;
    public Philosopher(int num, Object leftStick, Object rightStick){
        this.num = num;
        this.leftStick = leftStick;
        this.rightStick = rightStick;
    }

    @Override
    public void run(){
        while(true){
            synchronized (leftStick){
                System.out.println(num + ". philosopher took the left stick." );
                synchronized (rightStick){
                    System.out.println(num + ". philosopher took the right stick." );
                    snooze(); // The philosopher is eating
                    System.out.println(num + ". philosopher ate." );
                    System.out.println(num + ". philosopher put the right stick." );
                }
                System.out.println(num + ". philosopher put the left stick." );
            }
            System.out.println(num + ". philosopher is thinking." );
            snooze(); // The philosopher is thinking

        }
    }

    public void snooze(){
        Random random = new Random();
        long time = random.nextInt(1000);
        try {
            sleep(time);
        }
        catch(InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public static void main(String[] args) {
        try {
            Object[] sticks = new Object[Integer.parseInt(args[0])];
            for(int i = 0; i < sticks.length; i++)
                sticks[i] = new Object();
            Philosopher[] philosophers = new Philosopher[Integer.parseInt(args[0])];
            for(int i = 0; i< Integer.parseInt(args[0]); i++){
                philosophers[i] = new Philosopher(i, sticks[i], sticks[(i+1) % Integer.parseInt(args[0])] );
                philosophers[i].start();
            }
        }
        catch (NumberFormatException e){
            System.out.println("Wrong argument type");
        }

    }
}
